FROM node:8.5-alpine as theme
WORKDIR /src
COPY ./themes/portfolio .
RUN apk --no-cache add python2
RUN yarn
ENV NODE_ENV=production
RUN yarn run build

FROM alpine:latest
RUN apk --no-cache add curl
ENV VERSION 0.40.2
RUN mkdir -p /usr/local/src \
    && cd /usr/local/src \
    && curl -L https://github.com/gohugoio/hugo/releases/download/v${VERSION}/hugo_${VERSION}_linux-64bit.tar.gz | tar -xz \
    && mv hugo /usr/local/bin/hugo \
    && curl -L https://bin.equinox.io/c/dhgbqpS8Bvy/minify-stable-linux-amd64.tgz | tar -xz \
    && mv minify /usr/local/bin/
WORKDIR /src
COPY ./ .
COPY --from=theme /src ./themes/portfolio
EXPOSE 1313
