---
title: Jin Lab
summary: >-
  Custom WordPress theme for UCSD research lab which can be extended to other
  research lab websites.
date: 2018-08-29T11:14:35.346Z
website: 'https://jinlab.biology.ucsd.edu'
technologies:
  - icon: wordpress-icon
    name: WordPress
  - icon: php
    name: PHP
  - icon: laravel
    name: Blade
  - icon: sass
    name: Sass
image: 'https://ucarecdn.com/42110e3a-471e-4700-9170-8144fbbee6b1/'
---
## Project

I built a custom WordPress theme for a UCSD research lab. This may not be the most polished looking site, but the client was happy with the end result.

## Process

After consulting with the client, I went to work on a simple, but modern design. I started with the [Roots](https://roots.io/) starter theme for WordPress as usual. I created a backend interface using Advanced Custom Fields to add new researchers, publications, and gallery images to the site. All content can be managed by the client without any source code modifications. At the client's request, I implemented a header that fades between two images. As a challenge to myself I created this feature using CSS only.

## Challenges

Typically I manage the WordPress installs of my client's, but in this case UCSD had servers available. I was stoked I didn't have to manage the installation, however I did run into issues. I later learned that the Roots theme has an incompatible file structure with the WordPress theme uploader. This meant that every time I made even minor tweaks to the theme I had to send a zip to the server administrator so he could upload it using FTP. Of course I develop locally, but deploying to production became a slow process of email tag.
