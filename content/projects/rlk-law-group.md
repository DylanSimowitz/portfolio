---
title: RLK Law Group
summary: >-
  A quick and simple website for a law group in need of a website for an
  advertising campaign
date: 2018-08-29T19:53:20.821Z
website: 'https://rlklawgroup.com'
technologies:
  - icon: html-5
    name: HTML
  - icon: css-3
    name: CSS
  - icon: netlify
    name: Netlify
image: 'https://ucarecdn.com/cbb5b910-f922-47cc-93e2-0882f9520faf/'
---
## Project

A quick and simple website for a law group in need of a website for an advertising campaign. Created with the basic technologies of web development: HTML and CSS. I used Netlify for the deployment which makes it very easy to host static websites for free. I've included this project in my portfolio because it demonstrates responsive design.

## Process

There wasn't a whole lot to this project. The client needed a simple webpage ASAP as the hockey season was beginning shortly. I created a web page design and logo in an afternoon and had the site up and running within a day.
