---
title: Elements
summary: A React powered webapp which displays an interactive periodic table
date: 2018-08-29T11:28:49.400Z
website: "https://ptoe.herokuapp.com"
source: "https://gitlab.com/DylanSimowitz/elements"
technologies:
  - icon: react
    name: React
  - icon: redux
    name: Redux
image: "https://ucarecdn.com/845d8b47-6369-4de0-8958-c4ecbe1915b2/"
---

## Project

Interactive periodic table to visualize elemental data. The application uses React for the UI and Redux for the state management. I used styled-components to dynamically style the application on state change. SVG was used to create the visual atomic representations of atoms.

## Process

This was my first "big" React application I worked on. I really wanted to strengthen my knowledge of React and its ecosystem. I used popular React libraries including react-router, react-redux, and styled-components. I found styled-components to be a joy to work with and became a total believer of "css-in-js". Its so much easier to programatically change style properties than it is to create multiple classes and updating the className prop. Even though the Bohr atomic model isn't really that scientifically accurate, I thought it would be fun to make. The visualization is made of React components which render SVG elements. Using this approach I was able to dynamically render atoms by providing count of electrons that reside in each orbital.
