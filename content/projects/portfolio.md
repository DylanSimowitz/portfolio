---
title: Portfolio
summary: >-
  Custom designed portfolio website to showcase web development projects I
  created. It's the site you're browsing right now!
date: 2018-08-29T01:11:21.854Z
website: 'https://dylan.simowitz.com/portfolio'
source: 'https://gitlab.com/DylanSimowitz/hugo-portfolio-theme'
technologies:
  - icon: hugo.svg
    local: true
    name: Hugo
  - icon: sass
    name: Sass
  - icon: webpack
    name: Webpack
  - icon: html-5
    name: HTML
  - icon: gitlab
    name: Gitlab CI
image: 'https://ucarecdn.com/e0974c94-4ad7-49cb-a1ac-b1bc1e7b83dc/'
---
## Project

This is my personal portfolio which showcases the various projects I've worked on. The site is built using Hugo, a static site generator.

## Process

Building my portfolio site was both a necessity and an educational experience. I really wanted to experiment with a static site generator instead of a fullblown CMS. After researching various static site generators (SSG) I ended up choosing Hugo. I chose Hugo over other SSGs because of its expansive templating engine and detailed documentation.
