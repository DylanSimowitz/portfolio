---
title: Law Offices of Mark B. Simowitz
summary: >-
  Custom WordPress theme for a San Diego personal injury lawyer (who also
  happens to be my dad)
date: 2018-08-29T10:49:52.121Z
website: 'https://simowitz.com'
technologies:
  - icon: wordpress-icon
    name: WordPress
  - icon: php
    name: PHP
  - icon: laravel
    name: Blade
  - icon: sass
    name: Sass
  - icon: aws-s3
    name: AWS S3
image: 'https://ucarecdn.com/219caa6c-51e4-438f-a68e-47ef52fb387e/'
---
## Project

I was tasked with building an updated website for a law office. The website content is entirely editable from the WordPress dashboard allowing for nearly complete client management of the site (minus security). Lead generation via a form in the footer alerts the office of new client inquiries.

## Process

I'm a huge fan of the [Roots](https://roots.io/) starter theme for WordPress and use it as a starting point for every theme I build. I believe it offers superior code structure and templating to traditional WordPress themes. After discussing the types of content that would be on the site, I implemented post types for cases and testimonials. I used Advanced Custom Fields (ACF) to create the necessary fields in the post types. I used a few libraries for the design including Flickity, a carousel. I created my own grid system using SASS and flexbox to implement responsive design. I wouldn't do this again as there are plenty of grid systems out there which account for edge cases. Creating the dropdowns without JavaScript was a challenge and looking back at the code I did WAY to much SASS nesting. The images uploaded to the site are also stored on Amazon S3 because I was curious as to how that worked.
